local base = import 'base.libsonnet';
local defaultFileMode = 'u=rwX,g=rX,o=rX';

// {{{ builtin
local builtin = {
  // {{{ copy copy(src, dest, owner=null, group=null, mode=defaultFileMode)
  copy(src, dest, owner=null, group=null, mode=defaultFileMode):: base.task([
    {
      name: 'Copy "%s" to "%s"' % [src, dest],
      'ansible.builtin.copy': std.prune({
        src: src,
        dest: dest,
        owner: owner,
        group: group,
        mode: mode,
      }),
    },
  ]),
  // }}}

  template(src, dest, owner=null, group=null, mode=defaultFileMode):: base.task([
    {
      name: 'Template a  "%s" to "%s"' % [src, dest],
      'ansible.builtin.template': {
        src: src,
        dest: dest,
        owner: owner,
        group: group,
        mode: mode,
      },
    },
  ]),

  tempfile(state='file'):: base.task([
    {
      name: 'Create temporary directory',
      'ansible.builtin.tempfile': { state: state },
    },
  ]),

  file(path, owner=null, group=null, mode=defaultFileMode, state='file'):: base.task([
    {
      name: 'Change filesystem, state: %s path: "%s"' % [state, path],
      'ansible.builtin.file': {
        path: path,
        owner: owner,
        group: group,
        mode: mode,
        state: state,
      },
    },
  ]),

  removeDirectoryContents(path):: base.task([
    {
      name: 'Remove directory contents: %s' % path,
      'ansible.builtin.shell': 'rm -r %s/*' % path,
      changed_when: true,
      failed_when: false,
    },
  ]),

  getUrl(url, dest):: base.task([
    {
      name: 'Download url "%s" to "%s"' % [url, dest],
      'ansible.builtin.get_url': { url: url, dest: dest },
    },
  ]),

  unarchive(src, dest, remoteSrc=false):: base.task([
    {
      name: 'Extract archive "%s" to "%s"' % [src, dest],
      'ansible.builtin.unarchive': { src: src, dest: dest, remote_src: remoteSrc },
    },
  ]),

  waitFor(name='Waiting for condition', failMsg='Condition timeout exceeded', host='127.0.0.1', path=null, port=null, sleep=1, state='started', timeout=300):: base.task([
    {
      name: name,
      'ansible.builtin.wait_for': {
        host: host,
        msg: failMsg,
        path: path,
        port: port,
        sleep: sleep,
        state: state,
        timeout: timeout,
      },
    },
  ]),

  getent(database, key=''):: base.task([
    {
      name: 'Query getent database "%s" for "%s"' % [database, key],
      'ansible.builtin.getent': {
        database: database,
        key: key,
      },
    },
  ]),

  debug(var=null, msg=null):: base.task([
    {
      name: 'Print debug info for %s / %s' % [var, msg],
      'ansible.builtin.debug': std.prune({ msg: msg, var: var }),
    },
  ]),

  blockInFile(path, block, create=false, validate=null, insertBefore=null, mode=defaultFileMode):: base.task([
    {
      name: 'Ensure the file "%s" contents' % path,
      'ansible.builtin.blockinfile': {
        path: path,
        block: block,
        create: create,
        insertbefore: insertBefore,
        validate: validate,
        mode: mode,
      },
    },
  ]),

  lineInFile(path, regexp, line, state='present'):: base.task([
    {
      name: 'Ensure the file "%s" contains following line: %s' % [path, line],
      'ansible.builtin.lineinfile': {
        path: path,
        regexp: regexp,
        line: line,
        state: state,
      },
    },
  ]),

  apt(pkg=null, updateCache=null, upgrade=null):: base.task([
    {
      name: 'Performing apt update: %s; upgrade: %s; packages: %s' % [updateCache, upgrade, pkg],
      'ansible.builtin.apt': std.prune({
        upgrade: upgrade,
        update_cache: updateCache,
        pkg: pkg,
      }),
    },
  ]),

  aptKey(url):: base.task([
    {
      name: 'Install apt key from %s' % url,
      'ansible.builtin.apt_key': {
        url: url,
      },
    },
  ]),

  user(name, group=null, groups=null, append=null):: base.task([
    {
      name: 'Create unix user: %s' % name,
      'ansible.builtin.user': std.prune({
        name: name,
        group: group,
        groups: groups,
        append: append,
      }),
    },
  ]),

  service(name, state='started', enabled=null):: base.task([
    {
      name: 'Service %s, state: %s, enabled %s' % [name, state, enabled],
      'ansible.builtin.service': std.prune({
        name: name,
        state: state,
        enabled: enabled,
      }),
    },
  ]),

  systemd(daemonReload=true, scope='system'):: base.task([
    {
      name: 'Systemd daemon reload: %s' % daemonReload,
      'ansible.builtin.systemd': std.prune({
        daemon_reload: daemonReload,
        scope: scope,
      }),
    },
  ]),

  includeRole(name):: base.role([{
    name: 'Include role %s' % name,
    include_role: { name: name },
  }]),
};
// }}}

// {{{ podman
local podman = {
  containerTz: 'Europe/Warsaw',

  pod(name, state='started', ports=[], restartPolicy='unless-stopped'):: base.task([
    {
      name: 'Create pod "%s"' % name,
      'containers.podman.podman_pod': {
        name: name,
        state: state,
        ports: ports,
        restart_policy: restartPolicy,
      },
    },
  ]),

  network(name, state='present', internal=false, disable_dns=false, opt=null):: base.task([
    {
      name: 'Create podman network "%s"' % name,
      'containers.podman.podman_network': {
        name: name,
        state: state,
        internal: internal,
        disable_dns: disable_dns,
        opt: opt,
      },
    },
  ]),

  container(name,
            image=null,
            command=null,
            imageForcePull=false,
            env={},
            pod=null,
            ports=[],
            volumes=[],
            networks=[],
            dns=null,
            state='present',
            restartPolicy='unless-stopped',
            recreate=false,
            restart=false,
            privileged=false,
            userns=null,
            uidmap=null,
            gidmap=null,
            autoUpdate=false):: base.task(
    (if imageForcePull then [{
       name: 'Pulling image "%s"' % image,
       'containers.podman.podman_image': {
         name: image,
         force: true,
       },
     }]
     else [])
    + [
      (
        if state == 'present' then
          { name: 'Run container "%s"' % name }
        else if state == 'absent' then
          { name: 'Remove container "%s"' % name }
        else
          { name: 'Changing state of container "%s" to %s' % [name, state] }
      )
      +
      {
        'containers.podman.podman_container': std.prune({
          name: name,
          image: image,
          command: command,
          volumes: volumes,
          network: networks,
          dns: dns,
          ports: ports,
          env: { TZ: $.containerTz } + env,
          label: if autoUpdate then { 'io.containers.autoupdate': 'registry' } else {},
          pod: pod,
          state: state,
          privileged: privileged,
          userns: userns,
          gidmap: gidmap,
          uidmap: uidmap,
          restart_policy: restartPolicy,
          recreate: recreate,
          restart: restart,
        }),
      },
    ]
  ),

  volume(name, state='present'):: base.task([
    {
      name: 'Ensure podman volume "%s" is %s' % [name, state],
      'containers.podman.podman_volume': {
        name: name,
        state: state,
      },
    },
  ]),

  image(name, path, tag='latest', state='present', build={}):: base.task([
    {
      name: 'Preparing image "%s:%s" - state: %s' % [name, tag, state],
      'containers.podman.podman_image': {
        name: name,
        tag: tag,
        path: path,
        state: state,
        build: build,
        force: true,  // let podman do the caching instead - allows rebuilds on image parts change
      },
    },
  ]),

  mount(name):: base.task([
    {
      name: 'Mount container "%s"' % name,
      'ansible.builtin.shell': 'podman unshare podman mount ' + name,
    },
  ]),

  unmount(name):: base.task([
    {
      name: 'Unmount container "%s"' % name,
      'ansible.builtin.shell': 'podman unshare podman umount ' + name,
    },
  ]),

  inspectVolumeMountpoint(name):: base.task([
    {
      name: 'Register volume mount point for volume "%s"' % name,
      'ansible.builtin.shell': 'podman volume inspect --format "{{ \'{{\' }}.Mountpoint {{ \'}}\' }}" ' + name,
      changed_when: false,
    },
  ]),

  // Copy files using podman unshare
  copyFiles(src, dest, overwrite=true):: base.task([
    {
      name: 'Copy files from "%s" to "%s" with podman unshare' % [src, dest],
      'ansible.builtin.shell': |||
        podman unshare mkdir -p %(dest)s
        podman unshare cp -r %(noClobber)s %(src)s %(dest)s
      ||| % { src: src, dest: dest, noClobber: if overwrite then '' else '-n' },
      changed_when: true,
    },
  ]),

  exposePort(containerName, sourcePort, hostPort, networks=[])::
    podman.container(
      name=containerName + '-port-expose',
      image='docker.io/verb/socat',
      ports=['127.0.0.1:%d:1234' % hostPort],
      command='TCP-LISTEN:1234,reuseaddr,fork TCP:%s.dns.podman:%d' % [containerName, sourcePort],
      networks=['internal'],
      state='started'
    ),

  generate: {
    systemd(containerName):: base.task([
      {
        name: 'Create systemd unit file for replacing and restarting of container: %s' % containerName,
        'ansible.builtin.shell': |||
          mkdir -p $HOME/.config/systemd/user/
          cd $HOME/.config/systemd/user/
          podman generate systemd --new --files -n "%(containerName)s"
        ||| % { containerName: containerName },
        changed_when: true,
      },
    ]),
  },

  ubuntu: {
    addRepository():: (
      builtin.blockInFile(
        path='/etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list',
        block='deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_{{ ansible_lsb.release }}/ /',
        create=true
      )
      + builtin.aptKey('https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_{{ ansible_lsb.release }}/Release.key')
    ),
  },
};
// }}}

// {{{ postgres
local postgres = {
  loginUser: null,
  loginPassword: null,
  loginHost: 'localhost',
  loginPort: 5432,

  db(name, owner, state='present'):: base.task([
    {
      name: 'Create postgresql database "%s" with owner "%s" at "%s"' % [name, owner, $.loginHost],
      'community.postgresql.postgresql_db': {
        name: name,
        owner: owner,
        login_host: $.loginHost,
        login_port: $.loginPort,
        login_user: $.loginUser,
        login_password: $.loginPassword,
      },
    },
  ]),
  user(name, password, db=null, state='present'):: base.task([
    {
      name: 'Create postgresql user: "%s"' % name,
      'community.postgresql.postgresql_user': {
        name: name,
        db: db,
        password: password,
        state: state,
        login_host: $.loginHost,
        login_port: $.loginPort,
        login_user: $.loginUser,
        login_password: $.loginPassword,
      },
    },
  ]),
  ext(name, db, state='present'):: base.task([
    {
      name: 'Create postgresql extension: "%s"' % name,
      'community.postgresql.postgresql_ext': {
        name: name,
        db: db,
        state: state,
        login_host: $.loginHost,
        login_port: $.loginPort,
        login_user: $.loginUser,
        login_password: $.loginPassword,
      },
    },
  ]),
};
// }}}

// {{{ general
local general = {
  ufw(policy=null, proto=null, rule=null, state=null, port=null):: base.task([
    {
      name: 'Change firewall policy: "%s", proto: "%s", rule: "%s", state: "%s"' % [policy, proto, rule, state],
      'community.general.ufw': std.prune({
        state: state,
        policy: policy,
        proto: proto,
        rule: rule,
        port: port,
      }),
    },
  ]),
  pacman(pkgs=[]):: base.task([
    {
      name: 'Install packages: %s' % pkgs,
      'community.general.pacman': std.prune({
        name: pkgs,
      }),
    },
  ]),
};
// }}}

// {{{ posix
local posix = {
  sysctl(name, value=null, reload=true, state='present'):: base.task([{
    name: 'Change sysctl: %s=%s; state: %s, reload: %s' % [name, value, state, reload],
    'ansible.posix.sysctl': {
      name: name,
      value: value,
      reload: reload,
      state: state,
    },
  }]),
};
// }}}

// {{{ systemd
local systemd = {
  timer(name, description, command, timers, user='root'):: (
    builtin.template(
      src='templates/systemdunit.service.j2',
      dest='/etc/systemd/system/%s.service' % name
    ).vars({
      service_description: description,
      user: user,
      exec_start: command,
    })
    + builtin.template(
      src='templates/systemdunit.timer.j2',
      dest='/etc/systemd/system/%s.timer' % name
    ).vars({
      timer_description: description,
      timers: timers,
    })
  ),
  service(name, state='started', enabled=null, scope='system', daemonReload=false):: base.task([
    {
      name: 'Service %s, state: %s, enabled %s' % [name, state, enabled],
      'ansible.builtin.systemd': std.prune({
        name: name,
        state: state,
        enabled: enabled,
        scope: scope,
        daemon_reload: daemonReload,
      }),
    },
  ]),
};
// }}}

{
  podman: podman,
  postgres: postgres,
  builtin: builtin,
  general: general,
  posix: posix,
  systemd: systemd,
}
