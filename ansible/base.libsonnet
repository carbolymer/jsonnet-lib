{
  task(tasks):: {
    tasks+: tasks,
    map(f):: $.task(std.map(f, self.tasks)),
    register(name):: self.map(function(t) t { register: name }),
    become(user='root'):: self.map(function(t) t { become: true, become_user: user }),
    notify(handlers):: self.map(function(t) t { notify: handlers }),
    name(name):: self.map(function(t) t { name: name }),
    tags(tags=[]):: self.map(function(t) t { tags: tags }),
    vars(vars):: self.map(function(t) t { vars: vars }),
    with_fileglob(fileglob):: self.map(function(t) t { with_fileglob: fileglob }),
  },
  // loops over input, executing 'f' and concatenating tasks
  loopTask(f, input)::
    std.foldl(function(a, b) a + b, std.map(f, input) , {})
}
